<?php

/*
 * @file
 * Advance clock configuration form.
 */

namespace Drupal\advance_clock\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Description of AdvanceClockConfigForm
 *
 */
class AdvanceClockConfigForm extends ConfigFormBase {

  public function getFormId() {
    return 'advance_clock_config_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('advance_clock.settings');

    $form['advance_clock_count'] = array(
      '#type' => 'select',
      '#title' => t('Number of allowed clock per user'),
      '#options' => array(
        1 => t('1'),
        2 => t('2'),
        3 => t('3'),
        4 => t('4'),
        5 => t('5'),
        6 => t('6'),
      ),
      '#default_value' => $config->get('advance_clock_count'),
      '#description' => t('Allowed clock for user.'),
      '#required' => TRUE,
    );

    $form['advance_clock_show_secs'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Seconds'),
      '#default_value' => $config->get('advance_clock_show_secs'),
      '#description' => t('Show Seconds Counter with simple digital clock'),
    );

    $form['advance_clock_hour24'] = array(
      '#type' => 'checkbox',
      '#title' => t('24 hr\'s Clock.'),
      '#default_value' => $config->get('advance_clock_hour24'),
      '#description' => t('The Clock Type, Default is 12Hr Clock'),
    );

    $form['advance_clock_show_date'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Date'),
      '#default_value' => $config->get('advance_clock_show_date'),
    );

    $form['advance_clock_show_only_city'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show Only City Name'),
      '#default_value' => $config->get('advance_clock_show_only_city'),
      '#description' => t('Show only city name in clock\'s title.'),
    );

    $form['advance_clock_type'] = array(
      '#type' => 'radios',
      '#title' => t('Clock Type'),
      '#options' => array(
        0 => 'Digital Clock',
        1 => 'Analog Clock',
      ),
      '#default_value' => $config->get('advance_clock_type'),
      '#description' => t('Select which type of clock you want to display.'),
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->configFactory->get('advance_clock.settings');
    $form_state_values = $form_state->getUserInput();
    $config->set('advance_clock_count', $form_state_values['advance_clock_count']);
    $config->set('advance_clock_show_secs', $form_state_values['advance_clock_show_secs']);
    $config->set('advance_clock_hour24', $form_state_values['advance_clock_hour24']);
    $config->set('advance_clock_show_date', $form_state_values['advance_clock_show_date']);
    $config->set('advance_clock_type', $form_state_values['advance_clock_type']);
    $config->set('advance_clock_show_only_city', $form_state_values['advance_clock_show_only_city']);

    $config->save();
    drupal_set_message(t('Advance clock configuration saved successfully!'), 'status', FALSE);
  }

}
