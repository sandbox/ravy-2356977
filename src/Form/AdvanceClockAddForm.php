<?php
/**
 * @file
 * Add advance clock form
 */

namespace Drupal\advance_clock\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class AdvanceClockAddForm extends FormBase {

  public function getFormId() {
    return 'advance_clock_add_clock_form';
  }


  public function buildForm(array $form, FormStateInterface $form_state) {

    $user_clock = _advance_clock_get_user_clocks();

    $form['clock_zone'] = array(
      '#title' => t('Select Clock\'s Zones'),
      '#type' => 'checkboxes',
      '#options' => _advance_clock_get_clocks(),
      '#default_value' => $user_clock,
      '#required' => TRUE,
    );
    // Submit
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save',
    );
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory()->get('advance_clock.settings');
    $clock_zone = $form_state->getValue('clock_zone');
    $user_clocks = count(array_filter($clock_zone));

    if ($user_clocks > $settings->get('advance_clock_count')) {
      $form_state->setErrorByName('clock_zone', t('You can use @clock clock(s) allowed.', array('@clock' => $settings->get('advance_clock_count'))));
    }

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    $settings = $this->configFactory()->get('advance_clock.settings');
    $clock_zone = $form_state->getValue('clock_zone');
    $user_clocks = _advance_clock_get_user_clocks();

    $zones = implode(',', array_filter($clock_zone));

    $values = array(
      'uid' => $user->id(),
      'clock_zone' => $zones,
      'timestamp' => time(),
    );

    $db = \Drupal::database();
    if (count($user_clocks)) {
      //update clocks
      $query = $db->update('advance_clock')
        ->fields($values)
        ->condition('uid', $values['uid'], '=')
        ->execute();
    }
    else {
      $query = $db->insert('advance_clock')
        ->fields($values)
        ->execute();
    }
    if ($query == 1) {
      drupal_set_message('Your advance clock configuration is saved.');
    }
    else {
      drupal_set_message('Error in saving your advance clock configuration.', 'error');
    }

  }
}
