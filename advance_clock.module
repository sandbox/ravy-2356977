<?php

/**
 * @file
 */
/**
 * This module provides advanced clock block which is user configurable
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function advance_clock_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.advance_clock':
      return t('<h3> Welcome to Advance Clock help</h3> <p>As the name suggests this module gets you the advance and user configurable functionalities clock</p>
        <p>This module provides
        <ol> <li> A customizable clock block plugin.</li>
        <li> This module will also provide per user configurable clock.</li> </ol>
        </p>'
      );
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function advance_clock_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && !empty($plugin)) {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements of hook_theme().
 */
function advance_clock_theme($existing, $type, $theme, $path) {
  if ($type == 'module') {
    $templates = array(
      'analog_clock' => array(
        'variables' => array('images' => NULL),
        'template' => '/template/analog-clock',
      ),

      'digital_clock' => array(
        'variables' => array('images' => NULL),
        'template' => '/template/digital-clock',
      ),
    );
    return $templates;
  }
}

function _advance_clock_get_user_clocks() {
  $user = \Drupal::currentUser();
  $advance_user_clock = array();
  $connection = \Drupal::database();
  if ($user->id() != 0) {
    $query_advance_clock_list = $connection->select('advance_clock', 'ac');
    $query_advance_clock_list->fields('ac', array('clock_zone'));
    $query_advance_clock_list->condition('ac.uid', $user->id(), '=');
    $advance_clock_result = $query_advance_clock_list->execute()
      ->fetchAll();

    if (count($advance_clock_result)) {
      $advance_user_clock = explode(',', $advance_clock_result[0]->clock_zone);
    }
  }
  return $advance_user_clock;
}

function _advance_clock_get_clocks($blank = NULL) {
  $zonelist = timezone_identifiers_list();
  $zones = $blank ? array('' => t('- None selected -')) : array();
  foreach ($zonelist as $zone) {
    // Because many time zones exist in PHP only for backward compatibility
    // reasons and should not be used, the list is filtered by a regular
    // expression.
    if (preg_match('!^((Africa|America|Antarctica|Arctic|Asia|Atlantic|Australia|Europe|Indian|Pacific)/|UTC$)!', $zone)) {
      $timezoneTable[format_date(REQUEST_TIME, 'custom', ' O', $zone) / 100 . '|' . $zone] = t('(GMT@date) @zone,', array(
          '@zone' => t(str_replace('_', ' ', $zone)),
          '@date' => format_date(REQUEST_TIME, 'custom', ' O', $zone),
          '@d' => format_date(REQUEST_TIME, 'custom', ' O', $zone) / 100
        ));
    }
  }
  asort($timezoneTable);
  return $timezoneTable;
}

/**
 * hook_block_info.
 */
function advance_clock_block_info() {
  // This example comes from node.module.
  $blocks['advance_clock_user_clock'] = array(
    'info' => t('Clocks'),
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;
}

/**
 * hook_block_view.
 */
function advance_clock_block_view($delta = '') {
  // This example is adapted from node.module.
  $block = array();
  global $user;
  switch ($delta) {
    case 'advance_clock_user_clock':
      $block['subject'] = t('Clocks');
      $block['content'] = _advance_clock_clock_list();
      $block['content'] .= l('Configure Clocks', 'user/' . $user->uid . '/add-clock');
      break;
  }
  return $block;
}

function _advance_clock_clock_list() {
  $items = '';
  $js_elements = '';
  $users_clocks = _advance_clock_get_user_clocks();
  $module_path = drupal_get_path('module', 'advance_clock');
  $allowed_clock_count = variable_get('advance_clock_count');
  $show_date = variable_get('advance_clock_show_date');
  $show_secs = variable_get('advance_clock_show_secs');
  $show_city = variable_get('advance_clock_show_only_city');

  $configured_clocks = count($users_clocks);
  // @TODO: Show first n clocks for a selected value.
  if ($allowed_clock_count < $configured_clocks) {
    drupal_set_message('Please configure clocks', 'warning');
    $configured_clocks = $allowed_clock_count;
  }


  // Digital Clock (JQuery 1.5 and Abv).
  // @TODO: Add Language switcher.
  // @TODO: Date format made configurable.
  if (variable_get('advance_clock_simple_advance') == FALSE) {
    $output = '';
    $clocks = '';
    $module_path = drupal_get_path('module', 'advance_clock');
    $clock_type = variable_get('advance_clock_hour24');
    drupal_add_js($module_path . '/js/digitalClock.js');
    drupal_add_css($module_path . '/css/digital-clock.css');
    if ($configured_clocks != 0) {
      // Gets one single clock value.
      foreach ($users_clocks as $key => $zone) {
        $user_clock = explode('|', $zone);
        $user_location = $user_clock[1];
        //show only city name
        if ($show_city == TRUE) {
          $clock['city'] = explode('/', $user_location);
          $city = array_reverse($clock['city']);
          $city[0] = str_replace('_', ' ', $city[0]);
          $location = $city[0];
        }
        else {
          $location = str_replace('_', ' ', $user_location);
          $location = str_replace('/', ' / ', $location);
        }
        // JS.
        $varstoadd['clock_offset'][$key] = $user_clock[0];

        // Theme.
        $clock['clock_id'] = $key;
        $clock['format'] = $clock_type;
        $clock['location'] = $location;
        $clock['show_date'] = $show_date;
        $clock['show_secs'] = $show_secs;

        // Overrideable theme layer for single clock.
        $clocks .= theme('digital_clock', array('clock' => $clock));
      }

      // JS.
      $varstoadd['count'] = $key;
      $varstoadd['clock_type'] = $clock_type;
      drupal_add_js(array('advance_clock' => $varstoadd), 'setting');

      // Theme Wrapper.
      $output = '<div class="user-clocks">';
      $output .= $clocks;
      $output .= '</div>';
    }
  }

  // Code for analog/digital advance clock (using JS 1.10)
  elseif (variable_get('advance_clock_simple_advance') == TRUE) {
    $js_elements = '';
    $output = '';
    $clock_images = array();
    drupal_add_css('http://www.jqueryscript.net/css/jquerysctipttop.css', 'external');
    drupal_add_js('http://code.jquery.com/jquery-latest.min.js', 'external');
    drupal_add_js($module_path . '/js/jClocksGMT.js');
    drupal_add_js($module_path . '/js/jquery.rotate.js');
    drupal_add_css($module_path . '/css/jClocksGMT.css');
    if (count($users_clocks)) {
      foreach ($users_clocks as $zone) {
        $user_clocks = explode('|', $zone);
        $js_elements .= "$('#" . drupal_html_class($user_clocks[1]) . "').jClocksGMT({offset: '" . $user_clocks[0] . "', hour24: " . variable_get('advance_clock_hour24', FALSE) . ", digital: " . variable_get('advance_clock_digital', FALSE) . "});";
        $clock_hour = array(
          'path' => $module_path . '/images/clock_hour.png',
          'alt' => 'clock hour',
          'attributes' => array('class' => 'hour',),
        );
        $clock_min = array(
          'path' => $module_path . '/images/clock_min.png',
          'alt' => 'clock min',
          'attributes' => array('class' => 'min',),
        );
        $clock_sec = array(
          'path' => $module_path . '/images/clock_sec.png',
          'alt' => 'clock sec',
          'attributes' => array('class' => 'sec',),
        );
        $clock_face = array(
          'path' => $module_path . '/images/clock_face.png',
          'alt' => 'clock face',
          'attributes' => array('class' => 'clock',),
        );
        $clock_images['zone'] = str_replace("-", ",", $user_clocks[1]);
        $clock_images['id'] = drupal_html_class($user_clocks[1]);
        if (variable_get('advance_clock_analog', FALSE)) {
          $clock_images['hour'] = theme('image', $clock_hour);
          $clock_images['min'] = theme('image', $clock_min);
          $clock_images['sec'] = theme('image', $clock_sec);
          $clock_images['clock'] = theme('image', $clock_face);
        }
        $list[] = theme('advance_clock', array('images' => $clock_images));
      }
      $output = '<div class="user-clocks">';
      $output .= theme('item_list', array(
          'items' => $list,
          'attributes' => array('class' => 'clocks-list')
        ));
      $output .= '</div>';
      drupal_add_js("$(document).ready(function(){" . $js_elements . "});", 'inline');
    }
  }
  return $output;
}
